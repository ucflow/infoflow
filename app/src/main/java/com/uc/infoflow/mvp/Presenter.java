package com.uc.infoflow.mvp;


import com.uc.infoflow.RxBus;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * ****************************************************************************
 * Copyright (C) 2004 - 2017year UCWeb Inc. All Rights Reserved.
 * <p>
 * Description :
 * <p>
 * Creation    : 2017/6/10
 * Author   : Qiaowy
 * ****************************************************************************
 */
public class Presenter<T extends IView> implements IPresenter<T> {
    private T mView;
    private CompositeSubscription mCompositeSubscription;

    @Override
    public void attachView(T view) {
        this.mView = view;
        this.mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public void detachView() {
        mCompositeSubscription.clear();
        RxBus.getInstance().unSubscribe(mView);
    }

    protected void doOnEvent(Class<T> clazz, Action1<T> action){
        RxBus.getInstance().doOnEvent(mView, clazz, action);
    }
}
