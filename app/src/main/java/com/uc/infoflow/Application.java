package com.uc.infoflow;

import com.uc.infoflow.inject.component.ApplicationComponent;
import com.uc.infoflow.inject.component.DaggerApplicationComponent;
import com.uc.infoflow.inject.module.ApplicationModule;

/**
 * ****************************************************************************
 * Copyright (C) 2004 - 2017year UCWeb Inc. All Rights Reserved.
 * <p>
 * Description :
 * <p>
 * Creation    : 2017/7/4
 * Author   : Qiaowy
 * ****************************************************************************
 */
public class Application extends android.app.Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initApplicationComponent();
    }

    private void initApplicationComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
